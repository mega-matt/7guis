(ns widgets.routes
  (:require
   [bidi.bidi :as bidi]
   [pushy.core :as pushy]
   [re-frame.core :as re-frame]
   [widgets.events :as events]))

(defmulti panels identity)
(defmethod panels :default [] [:div "No panel found for this route."])

(defn maybe-slash [r]
  {"" r "/" r})

(def routes
  (atom
   ["/" {""      :home
         "about" :about
         "7guis/" {"counter" (maybe-slash :7guis/counter)
                   "temp" (maybe-slash :7guis/temp)
                   "flight-booker" (maybe-slash :7guis/flight-booker)
                   "task-timer" (maybe-slash :7guis/task-timer)
                   "crud" (maybe-slash :7guis/crud)
                   "circle" (maybe-slash :7guis/circle)}}]))

(defn parse
  [url]
  (bidi/match-route @routes url))

(defn url-for
  [& args]
  (apply bidi/path-for (into [@routes] args)))

(defn dispatch
  [route]
  (let [panel (keyword (namespace (:handler route)) (str (name (:handler route)) "-panel"))]
    (re-frame/dispatch [::events/set-active-panel panel])))

(defonce history
  (pushy/pushy dispatch parse))

(defn navigate!
  [handler]
  (pushy/set-token! history (url-for handler)))

(defn start!
  []
  (pushy/start! history))

(re-frame/reg-fx
  :navigate
  (fn [handler]
    (navigate! handler)))
