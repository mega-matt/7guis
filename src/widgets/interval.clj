(ns widgets.interval)

(defmacro interval [ms & body]
  `(let [close># (cljs.core.async/chan)]
     (cljs.core.async/go-loop
      []
       (cljs.core.async/alt!
         close># nil
         (cljs.core.async/timeout ~ms) (do (do ~@body)
                                           (recur))))
     (partial cljs.core.async/put! close># true)))
