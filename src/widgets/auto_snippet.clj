(ns widgets.auto-snippet)

(defmacro auto-snippet
  "surround this around forms to pprint them into a supplied atom before evaluation"
  [atm & [form :as forms]]
  (let [form-count (count forms)]
    `(do
       (reset! ~atm
               (with-out-str
                 (cljs.pprint/pprint (if (= 1 ~form-count)
                                       (quote ~form)
                                       (quote ~forms)))))
       ~@forms)))