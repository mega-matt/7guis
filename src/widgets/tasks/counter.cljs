(ns widgets.tasks.counter
  (:require
   [reagent.core :as r]
   [widgets.elements.input :refer [input]]
   [widgets.elements.button :refer [button]]
   [widgets.elements.gui-wrapper :refer [gui-wrapper]]
   [widgets.auto-snippet :refer [auto-snippet]]
   [widgets.utils :refer [cn]]))

(def code-holder (atom nil))
(auto-snippet code-holder
 (defn counter
   []
   (let [cur-val (r/atom 0)]
     (fn []
       [gui-wrapper "Counter" @code-holder
        [:div.flex.flex-row.justify-center
         [:div.flex.flex-col
          [input {:value @cur-val
                  :class (cn :w-full)
                  :disabled true
                  :on-change #()
                  :tab-index -1}]
          [button "Count" {:class (cn :mt-2)
                           :on-click (partial swap! cur-val inc)}]]]
        ::me]))))

