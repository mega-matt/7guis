(ns widgets.tasks.user-crud.events
  (:require
   [clojure.string :as str]
   [re-frame.core :refer [reg-event-db]]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [widgets.tasks.user-crud.core :as user-crud]))

(defn- clean-user [u]
  (-> u
      (update :first str/trim)
      (update :last str/trim)))

(reg-event-db
 ::user-crud/add-user
 (fn-traced
  [db [_ user]]
  (let [id (random-uuid)]
    (assoc-in db
              [::user-crud/users id]
              (-> user (assoc :id id) clean-user)))))

(reg-event-db
 ::user-crud/update-user
 (fn-traced
  [db [_ id user]]
  (update-in db [::user-crud/users id] #(merge % user))))

(reg-event-db
 ::user-crud/delete-user
 (fn-traced
  [db [_ id]]
  (update db ::user-crud/users dissoc id)))


