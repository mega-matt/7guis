(ns widgets.tasks.user-crud.components
  (:require
   [reagent.core :as r]
   [widgets.tasks.user-crud.core :as user-crud]
   [widgets.elements.input :refer [input]]
   [widgets.elements.button :refer [button]]
   [widgets.elements.select :refer [select]]
   [widgets.elements.gui-wrapper :refer [gui-wrapper]]
   [widgets.auto-snippet :refer [auto-snippet]]
   [widgets.tasks.user-crud.events]
   [widgets.tasks.user-crud.subs]
   [widgets.utils :refer [<sub >evt silence-evt cn]]
   [widgets.elements.utils :refer [parse-form-data]]))

(def ^:private code-holder (atom nil))

(defn- submitter [k n props]
  [button n (merge props
                   {:name :submit
                    :value k
                    :type "submit"
                    :coercer keyword})])

(auto-snippet
 code-holder

 (defmulti submit! :submit)
 (defmethod submit! :create
   [{:keys [first last]}]
   (>evt [::user-crud/add-user {:first first :last last}]))
 (defmethod submit! :delete
   [{:keys [id]}]
   (>evt [::user-crud/delete-user id]))
 (defmethod submit! :update
   [{:keys [id first last]}]
   (>evt [::user-crud/update-user id {:first first :last last}])) 

 (defn- val->uuid [v]
   (some-> (not-empty v) uuid))
 
 (defn crud-panel
   []
   (let [empty-user {:first "" :last ""}
         user-filter (r/atom "")
         edit-user (r/atom empty-user)
         cur-user (r/atom nil)]
     (fn []
       (let [filtered-users (<sub [::user-crud/filtered-users @user-filter])]
         [gui-wrapper "User Crud" @code-holder
          [:form.flex.flex-col.text-white
           {:on-submit (juxt silence-evt
                             (partial reset! edit-user empty-user)
                             (partial reset! cur-user nil)
                             #(submit! (parse-form-data %)))}
           [:div.flex.flex-row
            (when @cur-user
              [input {:type "hidden"
                      :name :id
                      :value @cur-user
                      :coercer val->uuid}])
            [input {:value @user-filter
                    :on-change/val (partial reset! user-filter)
                    :placeholder "Filter Prefix"
                    :coercer str
                    :class (cn :w-64)}]]
           [:div.flex.flex-row
            [select {:size 2
                     :class (cn :w-64 :h-48 :select-none)
                     :name :person
                     :on-click (partial swap! cur-user #(if-not (= %1 %2) %1 "") @cur-user)
                     :on-change/val (partial reset! cur-user)
                     :value (or @cur-user "")
                     :coercer val->uuid}
             (concat filtered-users [["" ""]])]
            [:div.flex.flex-col.pl-8
             [:div.flex.flex-row
              [input {:name :first
                      :placeholder "First Name"
                      :value (:first @edit-user)
                      :on-change/val (partial swap! edit-user assoc :first)}]]
             [:div.flex.flex-row
              [input {:name :last
                      :value (:last @edit-user)
                      :placeholder "Last Name"
                      :on-change/val #(swap! edit-user assoc :last %)}]]]]
           [:div.flex.flex-row.mt-2
            [submitter :create "Create"]
            [submitter :update "Update" {:class (cn :ml-4) :disabled (not @cur-user)}]
            [submitter :delete "Delete" {:class (cn :ml-4) :disabled (not @cur-user)}]]]
          ::me])))))