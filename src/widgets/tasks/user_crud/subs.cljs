(ns widgets.tasks.user-crud.subs
  (:require
   [clojure.string :as str]
   [re-frame.core :refer [reg-sub]]
   [widgets.tasks.user-crud.core :as user-crud]))


(reg-sub
 ::user-crud/all-users
 (fn [db]
   (vals (::user-crud/users db))))

(reg-sub
 ::user-crud/filter
 #(-> % ::user-crud/filter not-empty))

(reg-sub
 ::user-crud/sorted-formated-users
 :<- [::user-crud/all-users]
 (fn [users]
   (->> users
        (map (fn [{:keys [first last id]}]
                [id (str last ", " first)]))
        (sort-by second compare))))

(reg-sub
 ::user-crud/filtered-users
 :<- [::user-crud/sorted-formated-users]
 (fn [users [_ filter-string]]
   (letfn [(matches? [n] (if (not-empty (str n))
                           (str/starts-with? n filter-string)
                           true) )]
     (filter (comp matches? second) users))))
