(ns widgets.tasks.degree-converter
  (:require
   [widgets.utils :refer [cn evt->val to-fixed]]
   [widgets.elements.input :refer [input]]
   [widgets.elements.gui-wrapper :refer [gui-wrapper]]
   [widgets.auto-snippet :refer [auto-snippet]]
   [reagent.core :as r]))

(def input-classes
  (cn "mt-1" "block" "px-3" "py-2" "bg-white" "border" "border-slate-300"
      "rounded-md" "text-sm" "shadow-sm" "placeholder-slate-400" "outline-0"
      "focus:border-sky-500" "focus:ring-1" "focus:ring-sky-500" "disabled:bg-slate-50"
      "disabled:text-slate-500" "disabled:border-slate-200" "disabled:shadow-none"
      "invalid:border-rose-600" "invalid:text-rose-600" "focus:invalid:border-rose-600"
      "focus:invalid:ring-rose-600"))


(def code-holder (atom nil))
(auto-snippet
 code-holder
 (defn- real-num [v]
   (when-not (-> v str js/Number js/isNaN)
     (js/Number v)))

 (let [five-ninths (/ 5 9)
       converters {:f #(-> % (- 32) (* five-ninths))
                   :c #(-> % (/ five-ninths) (+ 32))}]
   (defn try-convert [type val & [decimals]]
     (let [num (real-num val)
           convert (converters type)]
       (some-> num convert (to-fixed decimals)))))

 (def ^:private input-pattern "^[\\-]?([0-9]?)+$")
 (defn- valid-input? [i]
   (let [i (str i)]
     (boolean (or (#{""} i)
                  (.match (str i) (new js/RegExp input-pattern))))))
 (def ^:private other-type
   {:f :c :c :f})

;; fahrenheit
 (defn degree-converter
   []
   (let [temp-store (r/atom {:c "" :f ""})
        ;; specified temp: update with raw value
        ;; other temp:     update with converted value (or cur val if format problem)
         updater (fn [cur temp-type next-val]
                   {temp-type next-val
                    (other-type temp-type) (or (try-convert temp-type next-val)
                                               (-> temp-type other-type cur))})
         set! (partial swap! temp-store updater)]
     (fn []
       (let [valid? (every? (comp valid-input? second) @temp-store)]
         [gui-wrapper "Temp Convert" @code-holder
          [:div.flex.flex-row.justify-center.text-white
           [:div.flex.flex-col.items-center
            [input {:value (-> @temp-store :f)
                    :on-change (comp (partial set! :f) evt->val)
                    :pattern input-pattern
                    :class input-classes}]
            [:span "Fahrenheight"]]
           [:div.font-bold.text-6xl.leading-3.mt-4.mx-4
            (if valid? "=" "≠")]
           [:div.flex.flex-col.items-center
            [input {:value (-> @temp-store :c)
                    :on-change (comp (partial set! :c) evt->val)
                    :pattern input-pattern
                    :class input-classes}]
            [:span "Celcius"]]]
          ::me])))))
