(ns widgets.tasks.timer
  (:require
   [widgets.interval :refer [interval]]
   [reagent.core :as r]
   ["react" :as react]
   ["luxon" :refer [DateTime]]
   [goog.string :as gstring]
   [goog.string.format]
   [widgets.elements.input :refer [input]]
   [widgets.elements.gui-wrapper :refer [gui-wrapper]]
   [widgets.auto-snippet :refer [auto-snippet]]
   [widgets.elements.button :refer [button]]))


(def key-frame (partial gstring/format "@keyframes progress-bar-%s {
   0% { width: %s%; }
   100% { width: 100%; }
 }"))

(def ^:private code-holder (atom nil))
(auto-snippet
 code-holder
 (defn- abs-diff-now-ms
   [dt]
   (-> dt .diffNow .toObject .-milliseconds abs))

 (def ^:private max-dir-secs 20)
 (def ^:private decimal-mult (js/Math.pow 10 1))
 (defn- ms->dur [ms] (* (/ ms 1000) decimal-mult))
 (defn- dur->ms [dur] (* (/ dur decimal-mult) 1000))

 (defn- fill-bar
   [start-percent ms-to-complete]
   (let [id (random-uuid)]
     [:<>
      [:style (key-frame id start-percent)]
      [:div.w-full.bg-gray-200.rounded-full.h-2.5.dark:bg-gray-700
       [:div.bg-blue-600.h-2.5.rounded-full
        {:style {:animation (str "progress-bar-" id " " ms-to-complete "ms linear")
                 :animation-fill-mode "both"}}]]]))

 (defn- fill-bar-tracker
   "Controlling component for fill-bar. React use-effect is nice for this."
   [props]
   (let [{:keys [id ms]} (js->clj props :keywordize-keys true)
         [elapsed set-elapsed!] (react/useState (atom {:e 0  :tick (.now DateTime :id id)}))]
     (letfn [(tick! [{:keys [e tick] :as val}]
               (assoc val
                        ;; never go backward or overshoot ms
                      :e (max e (min (+ e (abs-diff-now-ms tick)) ms))
                      :tick (.now DateTime)))]
       (react/useEffect
        ;; start tick interval
        #(interval 100 (swap! elapsed tick!))
        (array ms id elapsed))

       (react/useEffect
        ;; reset timer
        #(when (not= id (:id @elapsed))
           (set-elapsed! (atom {:e 0 :tick (.now DateTime) :id id})))
        (array id elapsed))

       (let [e (:e @elapsed)
             remaining-millis (- ms e)
             percent-filled (if (zero? e)
                              0
                              (* 100 (/ e ms)))]
         (r/as-element
          [fill-bar percent-filled remaining-millis])))))

 (defn timer []
   (let [id (r/atom 0)
         dur-ms (r/atom 0)]
     (fn []
       [gui-wrapper
        "Timer"
        @code-holder
        [:div.flex.flex-col
         [:> fill-bar-tracker {:id @id :ms @dur-ms}]
         [input {:type "range"
                 :min 0
                 :max (* max-dir-secs decimal-mult)
                 :value (ms->dur @dur-ms)
                 :on-change/val (partial reset! dur-ms)
                 :coercer (comp dur->ms js/Number)}]
         [:span.text-white.text-2xl.font-bold (/ @dur-ms 1000)]

         [button "Reset" {:on-click (partial swap! id inc)}]]
        ::me]))))