(ns widgets.tasks.flight-booker
  (:require
   ["luxon" :refer [DateTime]]
   [widgets.elements.input :refer [input]]
   [widgets.elements.button :refer [button]]
   [goog.string :as gstring]
   [goog.string.format]
   [widgets.elements.select :refer [select]]
   [widgets.elements.gui-wrapper :refer [gui-wrapper]]
   [widgets.auto-snippet :refer [auto-snippet]]
   [widgets.utils :refer [re-named-groups silence-evt cn]]
   [reagent.core :as r]))

(def code-holder (atom nil))
(auto-snippet
 code-holder
 (defn get-today [] (.startOf (.now DateTime) "day"))

(def ^:private date-format
  #"(?<year>\d{4})[\-\/](?<month>\d{1,2})[\-\/](?<day>\d{1,2})")

(defn day-iso->valid-date-time
  [iso-date]
  (let [date-time (some->>
                   iso-date
                   (re-named-groups date-format)
                   (map #(update % 1 js/Number))
                   (into {})
                   clj->js
                   (.fromObject DateTime))]
    (when (some-> date-time .-isValid) date-time)))

(def ^:private incorrect-date-msg
  "Incorrect date format. Please use YYYY-MM-DD")

(def round-trip? #{:round-trip})

(defn- depart-flight-error? [{:keys [depart]}]
  (cond
    (not (day-iso->valid-date-time depart)) incorrect-date-msg
    (< (day-iso->valid-date-time depart) (get-today)) "Departure must be today or later" ))

(defn- return-flight-error? [{:keys [type depart return]}]
  (let [return (day-iso->valid-date-time return)
        departure (day-iso->valid-date-time depart)]
    (when (round-trip? type)
      (cond
        (not return)
        incorrect-date-msg

        (< return departure)
        "Return date must be after departure date"))))

(defn flight-booker
  []
  (let [today (get-today)
        store (r/atom {:type :one-way
                       :depart (.toFormat today "yyyy-MM-dd")
                       :return (.toFormat today "yyyy-MM-dd")})
        val-setter #(fn [v] (swap! store assoc % v))
        last-booking (r/atom nil)]
    (fn []
      (let [el-w :w-48]
        [gui-wrapper "Flight Booker" @code-holder
         [:div.flex.justify-between
          [:form.border.border-1.border-white.p-6
           {:class (cn "w-5/12")
            :on-submit (juxt silence-evt #(reset! last-booking @store))}
           [:div.text-white.-mb-1 "Flight Type"]
           [select
            {:class (cn el-w)
             :coercer keyword
             :on-change/val (val-setter :type)
             :value (:type @store)}
            {:one-way "one-way flight"
             :round-trip "return flight"}]
           [:div.text-white.-mb-1.mt-2 "Departure Date"]
           [input
            {:class (cn el-w)
             :value (:depart @store)
             :validation-error (depart-flight-error? @store)
             :coercer str
             :on-change/val (val-setter :depart)}]
           [:div.text-white.-mb-1.mt-2 "Return Date"]
           [input
            {:class (cn el-w)
             :value (:return @store)
             :validation-error (return-flight-error? @store)
             :coercer str
             :on-change/val (val-setter :return)
             :disabled (-> @store :type round-trip? not)}]
           [:div.mt-6
            [button
             "book"
             {:class (cn el-w)}]]]
          (if-not @last-booking
            [:div]
            [:div.p-6.text-white
             {:class (cn "w-5/12")}
             (let [{:keys [depart return type]} @last-booking
                   booked (gstring/format "You have booked a %s trip." (if (= :one-way type) "one-way" "round-trip"))
                   depart (gstring/format "Your departure flight date is %s." depart)
                   return (when (= :round-trip type) (gstring/format "Your return flight date is %s." return))]
               [:<>
                [:div.text-lg "Thank you for booking"]
                [:div (str booked depart return)]])])
          ]
         ::me]))))

)

