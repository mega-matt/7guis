(ns widgets.tasks.circle
  (:require
   [reagent.core :as r]
   ["react" :as react]
   [goog.string :as gstring]
   [goog.string.format]
   [widgets.elements.gui-wrapper :refer [gui-wrapper]]
   [widgets.elements.button :refer [button]]
   [widgets.elements.input :refer [input]]
   [widgets.auto-snippet :refer [auto-snippet]]
   [widgets.utils :refer [cn silence-evt]]))

(def code-holder (atom nil))

(auto-snippet
 code-holder

 (defn- client-coord [evt]
   {:x (-> evt .-nativeEvent .-offsetX)
    :y (-> evt .-nativeEvent .-offsetY)})
 
 (defn capture-context-menu ;; aka right click
   [props]
   (let [{:keys [f el]} (js->clj props :keywordize-keys true)]
     (react/useEffect
      (fn []
        (let [handler #(when (= (.-target %)  @el)
                         (silence-evt %)
                         (f))]
          (.addEventListener js/document "contextmenu" handler)
          #(.removeEventListener js/document "contextmenu" handler)))
      (array el))
     (r/as-element nil)))

 (defn draw-circle
   [{:keys [x y radius id]} & [{:keys [on-click editting?]}]]
   (let [color "#7C3AED"
         me (atom nil)]
     [:ellipse.z-30
      {:ref (partial reset! me)
       :key id
       :fill-opacity (if editting? 0.5 1)
       :fill (if editting? color "transparent")
       :stroke color
       :stroke-width 2
       :cx x :cy y
       :rx radius :ry radius}
      [:> capture-context-menu {:f on-click :el me}]])) 
 
 (defn- update-circle
   [circles {:keys [id] :as circle}]
   (let [next-circles (update circles id merge circle)]
     (with-meta next-circles {:last circles})))
 
 (defn undo
   [circles]
   (if-let [last-c (:last (meta circles))]
     (vary-meta last-c assoc :next circles)
     circles))
 
 (defn redo
   [circles]
   (or (:next (meta circles)) circles))
 
 (defn- circle
   []
   (let [circles (r/atom {})
         update-circle! (partial swap! circles update-circle)
         undo! (partial swap! circles undo)
         redo! (partial swap! circles redo)
         edit-circle-store (r/atom nil)
         edit! (partial reset! edit-circle-store)
         apply-edit! #(do (update-circle! @edit-circle-store)
                          (reset! edit-circle-store nil))
         clear! (juxt (partial reset! circles {})
                      (partial reset! edit-circle-store nil))]
     (fn []
       (let [circles @circles
             edit-circle @edit-circle-store]
         [:div.flex.flex-col
          [:div.relative.action-frame
           ;; edit buttons
           [:div.pt-4.flex.flex-row.justify-center.pb-4.bg-violet-600
            [button "Undo" {:class (cn :mx-8 :w-24) :on-click undo!}]
            [button "Redo" {:class (cn :mx-8 :w-24) :on-click redo!}]
            [button "Clear" {:class (cn :mx-8 :w-24) :on-click clear!}]]

           ;; svg and edit svg
           [:div.relative.bg-black.w-full
            {:style {:height "600px"}
             :class (cn :h-96 :bg-neutral-900 :w-full)}
            [:svg.absolute.inset-0.h-full.w-full
             {:on-click #(update-circle! (merge (client-coord %) {:radius 25 :id (random-uuid)}))}
             (->> (dissoc circles (:id edit-circle))
                  vals
                  (map #(draw-circle % {:on-click (partial edit! %)}))
                  (doall))]
            (when edit-circle
              [:div.absolute.inset-0.cover.z-10.flex.flex-col.justify-center
               {:class (cn "bg-white/20")}
               [:div.z-20.w-full
                [:div.m-12.bg-white.p-4.rounded-lg
                 [:div.flex.flex-col.items-center.w-full
                  [:span (gstring/format "Adjust diameter of Circle at (%s, %s)" (:x edit-circle) (:y edit-circle))]
                  [input {:type "range"
                          :class (cn :w-full :z-20)
                          :min 1
                          :max 200
                          :value (:radius edit-circle)
                          :on-change/val (partial swap! edit-circle-store assoc :radius)
                          :coercer js/Number}]
                  [button "Save" {:class (cn :z-20) :on-click apply-edit!}]
                  [:svg.absolute.inset-0.w-full.h-full.z-10
                   [draw-circle edit-circle {:editting? true}]]]]]])]]])))))

(defn circle-panel
  []
  [gui-wrapper
   "Circle Drawer"
   @code-holder
   [circle]
   ::me
   true])