(ns widgets.events
  (:require
   [re-frame.core :as re-frame]
   [widgets.db :as db]
   [oops.core :refer [oset!]]
   [day8.re-frame.tracing :refer-macros [fn-traced]]))
   

(re-frame/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(re-frame/reg-event-fx
  ::navigate
  (fn-traced [{db :db} [_ handler color]]
   (-> js/document
       (.querySelector "#app #scroll-frame")
       (oset! :scrollTop 0))
   {:navigate handler
    :db (assoc db :layout-color color)}))

(re-frame/reg-event-fx
 ::set-active-panel
 (fn-traced [{:keys [db]} [_ active-panel]]
   {:db (assoc db :active-panel active-panel)}))
