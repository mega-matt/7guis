(ns widgets.elements.button
  (:require
   [clojure.string :as str]
   [widgets.utils :refer [cn]]
   [widgets.elements.input :refer [input]]
   [widgets.elements.utils :refer [compose-val-sub]]))

(def focus-classes
  (cn "focus:border-sky-500" "focus:ring-1" "focus:ring-sky-500"
      "focus:invalid:border-rose-600" "focus:invalid:ring-rose-600"))

(def text-classes
  (cn "mt-1" "block" "px-3" "py-2" "bg-white" "border" "border-slate-300"
      "rounded-md" "text-sm" "shadow-sm" "placeholder-slate-400" "outline-0"
      "disabled:bg-slate-50" "disabled:text-slate-500" "disabled:border-slate-200"
      "disabled:shadow-none" "invalid:border-rose-600" "invalid:text-rose-600" focus-classes))

(defn button
  [name props]
  [:button.text-neutral-900
   (-> props
       (assoc :type "submit")
       (update :class (comp str/join concat) " " text-classes)
       compose-val-sub
       (dissoc :validation-error))
   name])