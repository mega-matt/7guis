(ns widgets.elements.gui-wrapper
  (:require
   [clojure.string :as str]
   [cljs.core.async :refer [go-loop timeout <!]]
   [widgets.subs :as subs]
   [goog.string :as gstring]
   [goog.string.format]
   [widgets.utils :refer [<sub cn bg-color->border-color]]))

(defn- hljs-wait
  [el]
  (go-loop [retries 10]
    (if-not (.-hljs js/window)
      (do (<! (timeout 3000))
          (recur (dec retries)))
      (js/hljs.highlightElement el))))

(defn- highlight
  [code]
  (let [code (str/replace code #"(\w\d).+?\#" "$1")]
    [:pre {:ref #(some-> % hljs-wait)} [:code.language-clojure code]]))

(defn gui-wrapper
  [gui-name code children who? & [no-padding?]]
  (let [color (<sub [::subs/layout-color])
        bg-color (str "bg-" color)
        border-color (bg-color->border-color bg-color)
        path (some-> (namespace who?) (str/replace "." "/") (str/replace "-" "_"))]
    
    [:div.gui-wrapper.flex.flex-col.items-center.w-full
     [:span.text-lg.font-bold.mb-4 gui-name]
     [:div.shadow-lg.w-full.border-y-4
      {:class (cn "bg-neutral-500/90" border-color (when-not no-padding? :p-10))}
      [:div children]]
     [:div.p-2.my-4.flex.flex-col.text-md.font-bold
      [:span "macro generated code approximation"]
      (when path
        [:span "For real code see this file in "
         [:a.underline {:href (gstring/format "https://gitlab.com/mega-matt/7guis/-/blob/master/src/%s.cljs" path)
                        :target :_blank}
          "Gitlab"]])
      
      [highlight code]]]))