(ns widgets.elements.input
  (:require
   [clojure.string :as str]
   [widgets.utils :refer [cn]]
   [widgets.elements.utils :refer [validatable-element compose-val-sub]]))

(def focus-classes
  (cn "focus:border-sky-500" "focus:ring-1" "focus:ring-sky-500"
      "focus:invalid:border-rose-600" "focus:invalid:ring-rose-600"))

(def text-classes
  (cn "mt-1" "block" "px-3" "py-2" "bg-white" "border" "border-slate-300"
      "rounded-md" "text-sm" "shadow-sm" "placeholder-slate-400" "outline-0" 
      "disabled:bg-slate-50" "disabled:text-slate-500" "disabled:border-slate-200"
      "disabled:shadow-none" "invalid:border-rose-600" "invalid:text-rose-600" focus-classes))

(def range-classes
  (cn "mt-1" "blox"  "py-2" "bg-white" ))

(defn type->classes
  [type]
  (case type
    :range range-classes
    text-classes))

(defn input
  "Wrapper for themed and improved input.
   val-change: accepts a 1 or 2 arity fn where 1 is the value
               and 2 is the event (for stopping propagation etc)"
  [{error :validation-error :as props}]
  [validatable-element
   [:input.text-neutral-900
    (-> props
        (update :class (comp str/join concat) " " (-> props :type keyword type->classes))
        compose-val-sub
        (dissoc :validation-error))]
   error])