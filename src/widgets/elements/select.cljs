(ns widgets.elements.select
  (:require
   [widgets.utils :refer [cn]]
   [widgets.elements.utils :refer [compose-val-sub]]))

(defn opt [[value display]]
  [:option (cond-> {:key value :value value}
             (#{""} display)
             (assoc :style {:display "none"})) display])

(def classes
  (cn "mt-1" "block" "px-3" "py-2" "bg-white" "border" "border-slate-300"
      "rounded-md" "text-sm" "shadow-sm" "placeholder-slate-400" "outline-0"
      "focus:border-sky-500" "focus:ring-1" "focus:ring-sky-500" "disabled:bg-slate-50"
      "disabled:text-slate-500" "disabled:border-slate-200" "disabled:shadow-none"
      "invalid:border-rose-600" "invalid:text-rose-600" "focus:invalid:border-rose-600"
      "focus:invalid:ring-rose-600"))

(defn select
  [props opts]
  (apply vector :select
         (-> props
             (dissoc :opts)
             compose-val-sub
             (update :class str " text-neutral-900 " classes))
         (mapv opt opts)))