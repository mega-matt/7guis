(ns widgets.elements.utils
  (:require
   [oops.core :refer [oget]]
   [widgets.utils :refer [evt->val]]))

(def ^:private coercers (atom {}))

(defn get-coercer
  [uuid]
  (get @coercers uuid))

(defn compose-val-sub
  [{val-change :on-change/val coercer :coercer on-change :on-change
    :or {coercer identity}
    :as props}]
  (let [id (random-uuid)
        val-change (some-> val-change (comp coercer evt->val))]
    
    (when (not= identity coercer) (swap! coercers assoc id coercer))

    (cond-> (dissoc props :coercer :on-change/val)
      coercer
      (assoc :data-coercer-id id)

      val-change
      (assoc :on-change (juxt (or on-change #{})
                              val-change)))))

(defn validatable-element
  [child error-msg]
  [:div.contents {:ref #(some-> %
                       .-firstElementChild
                       (.setCustomValidity (or error-msg "")))}
   child])

(defn- coerce-by-name-val
  [start-el [name val]]
  [(keyword name)
   (or (some-> start-el
               (.querySelectorAll (str "[name=\"" name "\"]"))
               js/Array.from
               js->clj
               last
               .-dataset
               .-coercerId
               uuid
               get-coercer
               (apply [val]))
       val)])

(defn- add-submitter-val
  [evt data]
  (let [submitter (oget evt :nativeEvent :submitter)
        n (some-> submitter (oget :name))
        coercer (or
                 (some-> submitter (oget :?dataset :?coercerId) uuid get-coercer)
                 identity)
        value (some-> submitter (oget :value) coercer)]
    (cond-> data
      n
      (assoc (keyword n)
             (coercer value)))))

(defn parse-form-data
  [evt]
  (let [form (.-target evt)]
    (->> form
         (new js/FormData)
         js/Object.fromEntries
         js->clj
         ;;😢 chrome doesn't encode submit inputs
         (add-submitter-val evt)
         (map (partial coerce-by-name-val form))
         (into {})))
  )