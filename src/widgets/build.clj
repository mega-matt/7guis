(ns widgets.build
  (:require
   [teknql.tailwind :as tailwind]))

(def release? (System/getenv "COMPILE"))

(defn start-tailwind-watch!
  {:shadow.build/stage (if release? :flush :configure)}
  [build-state]
  (let [real-tailwind! (if release? tailwind/compile-release! tailwind/start-watch!)]
    (-> build-state
        (assoc-in [:shadow.build/config :devtools :http-root] (get-in build-state [:shadow.build/config :output-dir]))
        real-tailwind!)))
