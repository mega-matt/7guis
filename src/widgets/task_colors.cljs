(ns widgets.task-colors)

(def task-colors
  {:counter "sky-200"
   :temp "lime-100"
   :flight "green-300"
   :timer "orange-200"
   :crud "blue-400"
   :circle "violet-200"})

(def route->task-color
  (comp task-colors
        {:7guis/counter-panel :counter
         :7guis/temp-panel :temp
         :7guis/flight-booker-panel :flight
         :7guis/task-timer-panel :timer
         :7guis/crud-panel :crud
         :7guis/circle-panel :circle}))