(ns widgets.utils
  (:require
   ["classnames" :as classnames]
   [clojure.string :as str]
   [re-frame.core :as rf]))

(defmulti evt->val
  (fn [evt]
    (-> evt .-target .-tagName str/lower-case keyword)))

(defmethod evt->val :input
  [evt]
  (-> evt .-target .-value))

(defn bg-color->border-color
  [bg-color]
  (some-> bg-color name (str/replace "bg-" "border-") (str/replace #"\d\d\d" "600")))
(defmethod evt->val :select
  [evt]
  (let [multi? (-> evt .-target .-multiple)]
    (if-not multi?
      (-> evt .-target .-value)
      (->> evt
           .-target
           .-selectedOptions
           (mapv #(.-value %))))))

(defn silence-evt
  [evt]
  (.stopPropagation evt)
  (.preventDefault evt)
  silence-evt)

(defn cn [& params]
  (apply classnames (map clj->js params)))

(defn re-named-groups
  [re s]
  (some-> (.match s re)
          .-groups
          js/JSON.stringify
          js/JSON.parse
          (js->clj :keywordize-keys true)))

(defn to-fixed [n & [decimals]]
  (let [n (-> n str js/Number)
        pow (js/Math.pow 10 decimals)]
    (/ (js/Math.round (* pow n)) pow)))

(def <sub (comp deref rf/subscribe))
(def >evt rf/dispatch)