(ns widgets.subs
  (:require
   [re-frame.core :as re-frame]
   [widgets.task-colors :refer [route->task-color]]))

(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))

(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))

(re-frame/reg-sub
 ::layout-color
 :<- [::active-panel]
 (fn [panel]
   (or (route->task-color panel)
       "white")))

(re-frame/reg-sub
 ::on-home?
 :<- [::active-panel]
 (fn [panel]
   (= :home-panel panel)))