(ns widgets.views
  (:require
   [re-frame.core :as re-frame]
   [widgets.events :as events]
   [widgets.routes :as routes]
   [widgets.tasks.counter :refer [counter]]
   [widgets.tasks.degree-converter :refer [degree-converter]]
   [widgets.tasks.flight-booker :refer [flight-booker]]
   [widgets.tasks.timer :refer [timer]]
   [widgets.tasks.user-crud.components :refer [crud-panel]]
   [widgets.tasks.circle :refer [circle-panel]]
   [widgets.subs :as subs]
   [widgets.task-colors :refer [task-colors]]
   [widgets.utils :refer [cn <sub bg-color->border-color]]))

(defn tile [title color ico desc view]
  (let [theme [:md:w-64 :h-64 :p-8 :rounded-lg :text-neutral-900
               :transition :duration-200 :hover:scale-105 :cursor-pointer
               :m-8 :drop-shadow-lg
               "w-full"]]
    [:div.flex.flex-col {:class (cn theme (str "bg-" color))
                         :on-click #(re-frame/dispatch [::events/navigate view])}
     [:i {:class (cn ico :text-4xl :pb-2)}]
     [:span.text-2xl.font-bold.mb-4 title]
     [:span.text-sm desc]]))

(defn layout [children]
  (let [color (<sub [::subs/layout-color])
        on-home? (<sub [::subs/on-home?])
        bg-class (str "bg-" (name color))]
    [:div.h-full.flex.justify-center
     ;; todo: move this hack into tailwind config safe-list (tailwind doesn't support dynamic classes)
     {:data-colors "bg-sky-200 bg-violet-200 bg-white bg-sky-200 bg-lime-100 bg-green-300 bg-orange-200 bg-blue-400
                    bg-sky-200/75 bg-violet-200/75 bg-white/75 bg-sky-200/75 bg-lime-100/75 bg-green-300/75 bg-orange-200/75 bg-blue-400/75
                    bg-sky-600 bg-violet-600 bg-wh600 bg-sky-600 bg-lime-600 bg-green-600 bg-orange-600 bg-blue-600
                    border-sky-600 border-violet-600 border-wh600 border-sky-600 border-lime-600 border-green-600 border-orange-600 border-blue-600"
      :class (cn
              :bg-cover
              "bg-[url('/images/back-drop.png')]")}
     [:div#scroll-frame.text-neutral-600.overflow-y-scroll.duration-500.transition-colors.ease-out.overscroll-none.hide-scroll.relative
      {:style {:width "800px" :scrollbar-width "none" }
       :class (cn (str (name (or bg-class :bg-white)) "/75"))}
      [:div.underline.pl-6.pt-1.cursor-pointer.absolute
       {:on-click #(re-frame/dispatch [::events/navigate :home])}
       (when-not on-home? "<< Home")]
      [:div.flex.flex-col.items-center.pb-20.shrink-0
       [:div.flex.flex-row.mb-1.select-none.mt-8
        [:div.flex.flex-col.items-center
         [:i.fa-solid.fa-list-check.text-8xl.mr-6]
         [:span.text-6xl "7 GUIs"]]
        [:img.ml-8.rounded-md {:src "/images/me.png"
                               :width 170}]]
       [:div.divider {:class (cn (bg-color->border-color bg-class)
                                 :select-none :border-b :w-96 :h-0 :my-4)}]
       [:a.underline.select-none {:target :_blank
                                  :href "https://gitlab.com/mega-matt/7guis"} "Source Code"]
       [:span.text-4xl.select-none
        "Matt Meisberger (mega" [:img.inline-block {:src "/images/megaman.png" :width 20 :class "pb-0.5"}] "matt)"]
       [:a.underline {:href "mailto:matthewdaniel@pm.me"} "matthewdaniel@pm.me"]
       [:div.flex.justify-center.w-full
        
        children]]]])
  )

(defn home-panel []
  [:<>
   [:div
    [:div.flex.flex-row.flex-wrap.justify-center
     [tile
      "Counter"
      (task-colors :counter)
      [:fa-solid :fa-stairs]
      "Simple counter input linked to button incrementer"
      :7guis/counter]
     [tile
      "Temp Converter"
      (task-colors :temp)
      [:fa-solid :fa-temperature-high]
      "Celcius and Fahrenheit converter. Enter either celcius or fahrenheit to see the
           conversion in the corresponding temperature"
      :7guis/temp]]
    [:div.flex.flex-row.flex-wrap.justify-center
     [tile
      "Flight Booker"
      (task-colors :flight)
      [:fa-solid :fa-plane-departure]
      "Flight date picker for one-way and round-trip flights. Date selector
           with validation to ensure selection of dates that in valid."
      :7guis/flight-booker]
     [tile
      "Timer"
      (task-colors :timer)
      [:fa-solid :fa-timer]
      "A task timer with a slider to increase or decreate the active timer."
      :7guis/task-timer]]
    [:div.flex.flex-row.flex-wrap.justify-center
     [tile
      "CRUD"
      (task-colors :crud)
      [:fa-solid :fa-database]
      "A create, remove, update, delete database of users"
      :7guis/crud]
     [tile
      "Circle Drawer"
      (task-colors :circle)
      [:fa-regular :fa-circle]
      "Draw circles onto a canvas with ability to resize, undo, and redo."
      :7guis/circle]]]])

(defn gui-404 []
  [:div.pt-24 "todo: make gui"])


(defmethod routes/panels :home-panel [] [home-panel])

(defmethod routes/panels :7guis/counter-panel [] [counter])
(defmethod routes/panels :7guis/temp-panel [] [degree-converter])
(defmethod routes/panels :7guis/flight-booker-panel [] [flight-booker])
(defmethod routes/panels :7guis/task-timer-panel [] [timer])
(defmethod routes/panels :7guis/crud-panel [] [crud-panel])
(defmethod routes/panels :7guis/circle-panel [] [circle-panel])

;; about

(defn about-panel []
  [:div
   [:h1 "This is the About Page."]

   [:div
    [:a {:on-click #(re-frame/dispatch [::events/navigate :home])}
     "go to Home Page"]]])

(defmethod routes/panels :about-panel [] [about-panel])

;; main

(defn main-panel []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])]
    [layout (routes/panels @active-panel)]
    ))
